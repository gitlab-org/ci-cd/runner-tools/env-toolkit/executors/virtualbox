# VirtualBox executor

Set up a Debian based VM to use for the [VirtualBox
executor](https://docs.gitlab.com/runner/executors/virtualbox.html)
executor.

## Prerequisites

- [Virtualbox](https://www.virtualbox.org/)
- [`vboxmanage`](https://www.oracle.com/technical-resources/articles/it-infrastructure/admin-manage-vbox-cli.html)
  available in path.
- [Vagrnat](https://www.vagrantup.com/)

## Run

```shell
./up.sh
```

Copy printed config to your `config.toml`

## Cleanup

```shell
./down.sh
```

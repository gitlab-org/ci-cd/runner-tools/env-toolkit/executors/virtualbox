#!/usr/bin/env bash

vagrant up

vagrant halt

echo "Add the following to your config.toml file"
cat << EOF
  [runners.ssh]
    user = "vagrant"
    password = "vagrant"
    host = "192.168.33.10"
    identity_file = "$PWD/.vagrant/machines/default/virtualbox/private_key"
  [runners.virtualbox]
    base_name = "gitlab-runner-vb-executor"
    disable_snapshots = false
EOF

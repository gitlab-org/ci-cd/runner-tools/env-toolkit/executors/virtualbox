#!/usr/bin/env bash

vagrant halt

# Delete vms
vboxmanage list vms | grep 'gitlab-runner-vb-executor' | sed 's/^[^{]*{\([^{}]*\)}.*/\1/' | xargs -L1 VBoxManage unregistervm --delete

# Delete child mediums
vboxmanage list hdds | grep -B 4 'gitlab-runner-vb-executor-runner' | grep 'UUID' | grep -v 'Parent' | awk -F ':' '{print $2}' | xargs -L1 -I % vboxmanage closemedium disk % --delete

# Delete base mediums
vboxmanage list hdds | grep -B 4 'gitlab-runner-vb-executor' | grep 'UUID' | grep -v 'Parent' | awk -F ":" '{print $2}' | xargs -L1 -I % vboxmanage closemedium disk % --delete

